"use strict";
var express = require('express');
var session = require('cookie-session');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jade = require('jade');
var app = express();
var port=7000; // est pas 8080 sinon erreur



// mongodb
var mongoose = require("mongoose");
var ObjectID = require('mongodb').ObjectID;
let db=mongoose.connect("mongodb://localhost/mydb");
mongoose.connection.on("error", function() {
    console.log("erreur mongoose lol noob".bgRed)
})
mongoose.connection.on("open", function() {
    console.log("Mongoose OK".bgGreen)
})
var tacheSchema = mongoose.Schema({
    nom:String,
    dateFin:String,
    importance:String,
    partage:Boolean
})
let Tache=mongoose.model("taches", tacheSchema);


var userSchema = mongoose.Schema({
    nom:String,
    mdp: String
})
let Users=mongoose.model("users", userSchema);

var userTacheSchema = mongoose.Schema({
    iduser:String,
    idtache: String()
})
let UserTache=mongoose.model("userTache", userTacheSchema);

app.use(session({secret: 'motdepasse'}))

.use(function(req, res, next){
  if (typeof(req.session.todolist) == 'undefined') {
    req.session.todolist = [];
  }
  next();
})

.get('/todo/deconnect', function(req, res) {
  req.session.user = 'undefined';
  req.session.todolist = 'undefined';
  res.render('connect.jade');
})

.post('/todo/connect',urlencodedParser, function(req, res) {
  Users.find( function (err,users){
    users.forEach(
      function(user) {
        if(req.body.nom ==user['nom'] && req.body.password == user['mdp']){
          req.session.user = user;
        }
      }
    )
    if(req.session.user!='undefined'){
      res.redirect('/todo')
    }
    else{
      res.render('connect.jade');
    };30})


})

.post('/todo/inscription', urlencodedParser, function(req, res) {
  if (req.body.nom != ''  && req.body.password != '') {
      var user = new Users({nom: req.body.nom, mdp: req.body.password})
      req.session.user=user;
      user.save(function (err, user) {
         if (err) return console.error("ajouter")
       })
       res.redirect('/todo')
  }
  else{
    res.render('connect.jade');
  }
})

.get('/todo', function(req, res) {

  if (typeof(req.session.user) == 'undefined' && req.session.user!='undefined') {
    res.render('connect.jade');
  }
  else{
    UserTache.find(

      function (err,usersTaches){
        req.session.listeTache =new  Array();
        usersTaches.forEach(
          function(userTache) {
            if(userTache["iduser"]===req.session.user["_id"]){
              req.session.listeTache.push(userTache["idtache"].toString());
            }
          }
        )
      }
    );
    Tache.find( function (err,taches){
      req.session.todolist=[];
      if (err) return console.error("lol")
      taches.forEach(
        function(tache) {
          if(req.session.listeTache.indexOf(tache['_id'].toString()) > -1){
            req.session.todolist.push([tache['_id'],tache['nom'],tache['dateFin'],datelimit(tache['dateFin'])[0],datelimit(tache['dateFin'])[1],tache['importance'],tache['partage']]);
          }
        }
      );

    res.render('todo.jade',{todolist:req.session.todolist,lien:csvLien(req.session.todolist),nom:req.session.user['nom']});30})
  }

})

.get('/todo/tachesAutre', function(req, res) {
  Tache.find( function (err,taches){
    var listeTaches=new Array();
    if (err) return console.error("lol")
    taches.forEach(
      function(tache) {
        if(tache['partage']==true){
          listeTaches.push([tache['_id'],tache['nom'],tache['dateFin'],datelimit(tache['dateFin'])[0],datelimit(tache['dateFin'])[1],tache['importance'],tache['partage']]);
        }
      }
    );

  res.render('taches.jade',{todolist:listeTaches,lien:csvLien(listeTaches)});30})
})

.get('/todo/triC', function(req,res) {

  req.session.todolist.sort(
    function (a, b) {
      return a[1].localeCompare(b[1]);
  });
  res.render('todo.jade',{todolist:req.session.todolist,lien:csvLien(req.session.todolist)});
})


.get('/todo/triD', function(req,res) {
  req.session.todolist.sort(function (a, b) {
    return a[1].localeCompare(b[1]);
  }).reverse();
  res.render('todo.jade',{todolist:req.session.todolist,lien:csvLien(req.session.todolist)});
})

.get('/todo/triCroiDate', function(req,res) {
    req.session.todolist.sort(function(a, b) {

        a = new Date(a[2]);
        b = new Date(b[2]);
        return a>b ? -1 : a<b ? 1 : 0;
    })
  res.render('todo.jade',{todolist:req.session.todolist,lien:csvLien(req.session.todolist)});
})

.get('/todo/triDecDate', function(req,res) {
    req.session.todolist.sort(function(a, b) {

        a = new Date(a[2]);
        b = new Date(b[2]);
        return a>b ? -1 : a<b ? 1 : 0;
    }).reverse()
  res.render('todo.jade',{todolist:req.session.todolist,lien:csvLien(req.session.todolist)});
})

.get('/todo/importance/:importance', function(req, res) {
  var listeImportance = new Array();
  for(var i = 0;i<req.session.todolist.length;i++){
    if(req.session.todolist[i][5]==req.params.importance){
      listeImportance.push(req.session.todolist[i]);
    }
  }
  res.render('todo.jade',{todolist:listeImportance,lien:csvLien(req.session.todolist)});
})

.post('/todo/ajouter', urlencodedParser, function(req, res) {
        if (req.body.newtodo != '') {
            var tache = new Tache({nom: req.body.newtodo, dateFin: req.body.date_limit,importance:req.body.important,partage:req.body.publier})
            tache.save(function (err, tache) {
               if (err) return console.error("ajouter")
             })

             var userTache = new UserTache({iduser:req.session.user['_id'],idtache: tache["_id"]})

             userTache.save(function (err, userTache) {
                if (err) return console.error("ajouter")
              })
        }
        res.redirect('/todo')
    })

.get('/todo/ajouterTache/:id', function(req, res) {
  var deja = false;
  UserTache.find(
    function (err,usersTaches){
      req.session.listeTache =new  Array();
      usersTaches.forEach(
        function(userTache) {
          if(userTache["iduser"]===req.session.user["_id"] && userTache["idtache"]===req.params.id){
            deja=true;
          }
        }
      )
    }
  );
  if(deja){
    res.redirect('/todo');
  }
  else{
    var userTache = new UserTache({iduser:req.session.user['_id'],idtache: req.params.id})

    userTache.save(function (err, userTache) {
       if (err) return console.error("ajouter")
     })
     res.redirect('/todo');
  }


})

.get('/todo/supprimer/:id', function(req, res) {

  Tache.remove({_id: req.params.id}, function(err) {if (err) {console.log( ('erreur suppression '+req.params.id).bgRed )} else { console.log( ('suppression reussie '+req.params.id).bgGreen ) }})
  res.redirect('/todo');
})

.post('/todo/modif/:id', urlencodedParser,  function(req, res){
        //   Tache.update(
        //    { _id: req.params.id },
        //      { $set:
        //        {nom: req.body.newtodo, dateFin: req.body.date_limit,importance:req.body.important,partage:req.body.publier}
        //     }
        //  )

            var tache = new Tache({nom: req.body.updatetodo, dateFin: req.body.date_limit,importance:req.body.important,partage:req.body.publier})
            var upsertData = tache.toObject();
            delete upsertData._id;
            tache.save(function (err, tache) {
               if (err) return console.error("ajouter")
             })
            Tache.remove({_id : req.params.id}, function (err) {
                if (err) console.log(('error while updating '+req.params.id).bgRed)
            })

            var userTache = new UserTache({iduser:req.session.user['_id'],idtache: tache['_id']});
            userTache.save(function (err, userTache) {
               if (err) return console.error("ajouter")
             })
            // UserTache.update({iduser:req.session.user['_id'],idtache: tache['_id']}, upsertData, {upsert: true}, function(err){ if (err) {console.log( 'error  ')} });
            UserTache.remove({idtache : req.params.id}, function (err) {
                if (err) console.log(('error while updating '+req.params.id).bgRed)
            })
  res.redirect('/todo');
})

.get('/todo/modifTemplate/:id', urlencodedParser,  function(req, res){
  var id = req.params.id;

  Tache.find( function (err,taches){
    var tacheSelect;
      if (err) return console.error("lol")
    taches.forEach(function(tache) {
      if(tache['_id']==id)
      tacheSelect = [tache['_id'],tache['nom'],tache['dateFin'],datelimit(tache['dateFin'])[0],datelimit(tache['dateFin'])[1],tache['importance'],tache['partage']];
    });
  res.render('modifier.jade', {tache: tacheSelect, id: id});30})
})

.use(function(req, res, next){
  res.redirect('/todo');
})

.listen(port, function () {console.log("Serveur à l'écoute sur le port %d", port);});

function datelimit(date){
  var now = new Date();
  var annee   = now.getFullYear();
  var mois    = now.getMonth() + 1;
  var jour    = now.getDate();
  var laDate = new Date(date);
  // console.log(dateDiff(laDate,now));
  if(laDate<now){
    return [true,dateDiff(now,laDate)];

  }
  else{
    return [false,dateDiff(now,laDate)];
  }
}

function dateDiff(date1, date2){
    var diff = {}                           // Initialisation du retour
    var tmp = date2 - date1;

    tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
    diff.sec = tmp % 60;                    // Extraction du nombre de secondes

    tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
    diff.min = tmp % 60;                    // Extraction du nombre de minutes

    tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
    diff.hour = tmp % 24;                   // Extraction du nombre d'heures

    tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
    diff.day = tmp;

    return diff;
}


function csvLien(liste){
  var data=liste;
  var csvContent = "data:text/csv;charset=utf-8,";
  for( var i =0;i< data.length;i++){
    csvContent += data[i][1]+","+data[i][2]+","+data[i][5]+"\n";
  }
  var encodedUri = encodeURI(csvContent);
  return encodedUri;
}
